package harschware.sandbox.drivers;

import java.util.regex.Matcher

/**
 * 
 * This script will download and parse the HTML page that describes what files from
 * the dbpedia download dumps are loaded into the public dbnpedia
 * endpoint (found at: http://dbpedia.org/sparql).  The output file can be used as input
 * to wget to download the files necessary to create a mirror of dbpedia sparql endoint
 * 
 * NOTE: This script was developed when dbpedia relase 3.9 was the current.  
 * The datasets link below ( http://wiki.dbpedia.org/DatasetsLoaded) redirects to
 * http://wiki.dbpedia.org/Downloads39
 * 
 * @author tharsch
 *
 */
class DownloadDbpediaMirror {
	def static DATASETS_URL = 'http://wiki.dbpedia.org/DatasetsLoaded'.toURL();
	def static DBPEDIA_DOWNLOADS_BASE = 'http://downloads.dbpedia.org/3.9/';

	public static void main(String ...  args ) {
		//String xmlString = this.getClass().getResource( '/DatasetsLoaded39.html' ).text
		//println xmlString;

		println "**** downloading and parsing list of dbedia files ****";

		//File xmlFile  = new File(this.getClass().getResource( '/DatasetsLoaded39.html' ).getFile());
		def xmlString = DATASETS_URL.text; // download the contents into xmlString
		def slurpedXml = new XmlSlurper(false,false,true).parseText(xmlString) // 3rd param says don't barf on DocType

		def ul = slurpedXml.'**'
				.findAll{node-> node.name() == 'ul' }; // find all the ul nodes

		def bigListIdx = ul.findIndexOf{ it.li.size() >= 20 };  // a large number of li tags to hold the download files
		def theUl = ul[bigListIdx];

		def liValues = theUl.li*.text(); // collect the values of all li nodes

		// collect the values of all li nodes
		def liValues2 = liValues.collect{ it.trim() };

		// correct for incorrect filenames in list (data in the wild is dirty)
		def liValues3 = liValues2.collect{
			(it =~ /(.*)\/(infobox_properties_en)$/ || it =~ /(.*)\/(infobox_property_definitions_en)$/ ) ?
					"${Matcher.lastMatcher[0][1]}/raw_${Matcher.lastMatcher[0][2]}" : it
		}.collect{ it + ".nt.bz2" } ;

		def outputFilename = new File("dbPedia_miror_files.txt").getCanonicalPath();
		println "**** writing out results to ${outputFilename} ****";

		new File(outputFilename).withWriter { out ->
			liValues3.sort().each() {  out.writeLine(it) }
		}

		println "now go execute this command to download it all:"
		println "  wget -i ${outputFilename} --base \"$DBPEDIA_DOWNLOADS_BASE\" --force-directories"
	}
}
